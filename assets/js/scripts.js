/** TIMER PARAM
 *
 *  Format of date: MOUNTH DAY, YEAR HOURS:MINUTES:SECONDS 
 *
 */

 
// Don't forget to change the Dates
var start = "January 1, 2012 00:00:00";
var end   = "September 1, 2014 00:00:00"; 


function updateTime(time) {
	time.splice(0, 3);
	
	var ar = [];
	$.each(time, function(k, v) { ar.push(v.toString()) });
		
	var nums = [];
	$.each(ar, function(k, v){
		switch(k) {
			case 0:
				switch (v.length) {
					case 0: v = '000'; break;								
					case 1: v = '00'+v; break;
					case 2: v = '0'+v; break;
				}
			break;
			default:
				switch (v.length) {
					case 0: v = '00'; break;								
					case 1: v = '0'+v; break;
				}
			break;
		}
		
		$.each(v, function(key, val){ nums.push(val) });
	});
	
	
	$('#countdown > div > div > div').each(function(k) {
		var obj = $(this).find('span');
		
		if(obj.eq(0).text() == nums[k]) return true;
		
		/** Animate numbers **/
		obj.eq(1)
			.text(nums[k])
			.animate({opacity:1});
		obj.eq(0).css( "color", "#00B6A0" ) 
		obj.eq(0).animate({'margin-top': -88, opacity:0}, 666, function(){ 
			var parent = $(this).parent();
			$(this).remove();
			$('<span/>').css({opacity:0}).appendTo(parent);
		});
	});
	
	
}
// Preloader 
$(window).load(function() { // makes sure the whole site is loaded
	$('#status').fadeOut(); // will first fade out the loading animation
	$('#preloader').delay(700).fadeOut('slow'); // will fade out the white DIV that covers the website.
	$('body').delay(700).css({'overflow':'visible'});
})

jQuery(document).ready(function() {

	"use strict"
	
	
	var colors = new Array(
	[62,35,255],
	[60,255,60],
	[255,35,98],
	[45,175,230],
	[255,0,255],
	[255,128,0]);
	//Animated background gradient - http://codepen.io/quasimondo/pen/lDdrF
	var step = 0;
	//color table indices for: 
	// current color left
	// next color left
	// current color right
	// next color right
	var colorIndices = [0,1,2,3];

	//transition speed
	var gradientSpeed = 0.002;

	function updateGradient()
	{
	var c0_0 = colors[colorIndices[0]];
	var c0_1 = colors[colorIndices[1]];
	var c1_0 = colors[colorIndices[2]];
	var c1_1 = colors[colorIndices[3]];

	var istep = 1 - step;
	var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
	var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
	var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
	var color1 = "#"+((r1 << 16) | (g1 << 8) | b1).toString(16);

	var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
	var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
	var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
	var color2 = "#"+((r2 << 16) | (g2 << 8) | b2).toString(16);

	 $('#gradient').css({
	   background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
		background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});
	  
	  step += gradientSpeed;
	  if ( step >= 1 )
	  {
		step %= 1;
		colorIndices[0] = colorIndices[1];
		colorIndices[2] = colorIndices[3];
		
		//pick two new target color indices
		//do not pick the same as the current one
		colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
		colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
		
	  }
	}

	setInterval(updateGradient,10);

	// slideshow function
	var header;
	var switchBtnn;
	var toggleBtnn;
	var toggleCtrls;
	var toggleCompleteCtrls;
	var slideshow;
	var toggleSlideshow;
	
		header = document.getElementById( 'header' )
		switchBtnn = header.querySelector( 'button.slider-switch' ),
		toggleBtnn = function() {
			if( slideshow.isFullscreen ) {
				classie.add( switchBtnn, 'view-maxi' );
			}
			else {
				classie.remove( switchBtnn, 'view-maxi' );
			}
		},
		toggleCtrls = function() {
			if( !slideshow.isContent ) {
				classie.add( header, 'hide' );
			}
		},
		toggleCompleteCtrls = function() {
			if( !slideshow.isContent ) {
				classie.remove( header, 'hide' );
			}
		},
		slideshow = new DragSlideshow( document.getElementById( 'slideshow' ), { 
			// toggle between fullscreen and minimized slideshow
			onToggle : toggleBtnn,
			// toggle the main image and the content view
			onToggleContent : toggleCtrls,
			// toggle the main image and the content view (triggered after the animation ends)
			onToggleContentComplete : toggleCompleteCtrls
		}),
		toggleSlideshow = function() {
			slideshow.toggle();
			toggleBtnn();
		};
					
	// manual add class function
	$( ".menu li > a" ).addClass( "b_radius" );
	$( ".subscribe > h3" ).addClass( "m_bottom_20" );
	$( ".subscribe > form" ).addClass( "m_top_30" );
	
	var myModal2;
	var myModal3;
	var myModal4;
	// Popup boxes
	$('#myModal').jBox('Modal', {
		title: 'Grab an element',
		content: $('#grabMe')
	});
	
	// Timer setting
	$("#timer").countdown({
		until: new Date(end), 
		compact: true,
		onTick: updateTime
	});	

    // Newsletter
    $('.success-message').hide();
    $('.error-message').hide();

	myModal2 = $('#myModal2').jBox('Modal', {
		title: ' <h3>Subscribe to our newsletter</h3>',
		content: $('#grabMe1')
	});
	myModal3 = $('#myModal3').jBox('Modal', {
		title: '<h3>Contact</h3>',
		content: $('#grabMe2')
	});
	myModal4 = $('#myModal4').jBox('Modal', {
		title: '<h3>About Maintenance / Us </h3>',
		content: $('#grabMe3')
	});
	
	//************************* Animated texts
	$('.tlt').textillate({
	  // the default selector to use when detecting multiple texts to animate
	  selector: '.texts',

	  // enable looping
	  loop: false,

	  // sets the minimum display time for each text before it is replaced
	  minDisplayTime: 1000,

	  // sets the initial delay before starting the animation
	  // (note that depending on the in effect you may need to manually apply 
	  // visibility: hidden to the element before running this plugin)
	  initialDelay: 0,

	  // set whether or not to automatically start animating
	  autoStart: true,

	  // custom set of 'in' effects. This effects whether or not the 
	  // character is shown/hidden before or after an animation  
	  inEffects: [],

	  // custom set of 'out' effects
	  outEffects: [ 'hinge' ],

	  // in animation settings
	  in: {
		// set the effect name
		effect: 'fadeInLeft',

		// set the delay factor applied to each consecutive character
		delayScale: 1.5,

		// set the delay between each character
		delay: 50,

		// set to true to animate all the characters at the same time
		sync: false,

		// randomize the character sequence 
		// (note that shuffle doesn't make sense with sync = true)
		shuffle: false,

		// reverse the character sequence 
		// (note that reverse doesn't make sense with sync = true)
		reverse: false,

		// callback that executes once the animation has finished
		callback: function () {}
	  },

	  // out animation settings.
	  out: {
		effect: 'hinge',
		delayScale: 1.5,
		delay: 50,
		sync: false,
		shuffle: false,
		reverse: false,
		callback: function () {}
	  },

	  // callback that executes once textillate has finished 
	  callback: function () {}
	});
    $('.subscribe form').submit(function() {
        var postdata = $('.subscribe form').serialize();
        $.ajax({
            type: 'POST',
            url: 'assets/sendmail/newsletter.php',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                if(json.valid == 0) {
                    $('.success-message').hide();
                    $('.error-message').hide();
                    $('.error-message').html(json.message);
                    $('.error-message').fadeIn();
                }
                else {
                    $('.error-message').hide();
                    $('.success-message').hide();
                    $('.subscribe form').hide();
                    $('.success-message').html(json.message);
                    $('.success-message').fadeIn();
                }
            }
        });
        return false;
    });
	
	// contact form

		(function(){

			var cf = $('#contactform');
			cf.append('<div class="message_container d_none m_top_20"></div>');

			cf.on("submit",function(event){

				var self = $(this),text;

				var request = $.ajax({
					url:"assets/sendmail/mail.php",
					type : "post",
					data : self.serialize()
				});

				request.then(function(data){
					if(data == "1"){

						text = "Your message has been sent successfully!";

						cf.find('input:not([type="submit"]),textarea').val('');

						$('.message_container').html('<div class="alert_box r_corners color_green success"><i class="fa fa-smile-o"></i><p>'+text+'</p></div>')
							.delay(150)
							.slideDown(300)
							.delay(4000)
							.slideUp(300,function(){
								$(this).html("");
							});

					}
					else{
						if(cf.find('textarea').val().length < 20){
							text = "Message must contain at least 20 characters!"
						}
						if(cf.find('input').val() == ""){
							text = "All required fields must be filled!";
						}
						$('.message_container').html('<div class="alert_box r_corners error"><i class="fa fa-exclamation-triangle"></i><p>'+text+'</p></div>')
							.delay(150)
							.slideDown(300)
							.delay(4000)
							.slideUp(300,function(){
								$(this).html("");
							});
					}
				},function(){
					$('.message_container').html('<div class="alert_box r_corners error"><i class="fa fa-exclamation-triangle"></i><p>Connection to server failed!</p></div>')
							.delay(150)
							.slideDown(300)
							.delay(4000)
							.slideUp(300,function(){
								$(this).html("");
							});
				});


				event.preventDefault();
			});

		})();
		
		
// ==========
// Google Map
// ==========

if ($('#map').hasClass('gmap')) {
	$('.gmap').mobileGmap();
}

});


/**
 * jQuery Mobile Google maps
 * @Author: Jochen Vandendriessche <jochen@builtbyrobot.com>
 * @Author URI: http://builtbyrobot.com
 *
 * @TODO:
 * - fix https image requests
**/

(function($){
	"use strict";

	var methods = {
		init : function(config) {
			var options = $.extend({
				deviceWidth: 480,
				showMarker: true,
			}, config),
			settings = {},
			markers = [];
			// we'll use the width of the device, because we stopped browsersniffing
			// a long time ago. Anyway, we want to target _every_ small display
			var _o = $(this); // store the jqyuery object once
			// iframe?
			//<iframe width="425" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.be/maps?f=q&amp;source=s_q&amp;hl=nl&amp;geocode=&amp;q=Brugse+Heirweg+37,+aartrijke&amp;aq=&amp;sll=51.122175,3.086483&amp;sspn=0.009253,0.021651&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=Brugse+Heirweg+37,+8211+Zedelgem,+West-Vlaanderen,+Vlaams+Gewest&amp;t=m&amp;z=14&amp;ll=51.122175,3.086483&amp;output=embed"></iframe>
			options.imgURI = 'http://maps.googleapis.com/maps/api/staticmap?';
			settings.center = 'Brussels Belgium';
			settings.zoom = '5';
			settings.size = screen.width + 'x' +  480;
			settings.scale = window.devicePixelRatio ? window.devicePixelRatio : 1;
			settings.maptype = 'terrain';
			settings.sensor = false;
			options.settings = settings;

			if ($(this).attr('data-center')){
				options.settings.center = $(this).attr('data-center').replace(/ /gi, '+');
			}
			if ($(this).attr('data-zoom')){
				options.settings.zoom = parseInt($(this).attr('data-zoom'));
			}
			if ($(this).attr('data-maptype')){
				options.settings.zoom = $(this).attr('data-maptype');
			}
			
			// if there should be more markers _with_ text an ul.markers element should be used so
			// we can store all markers :-) (marker specific settings will be added later)
			if (options.showMarker){
				markers.push({
					label: 'A',
					position: settings.center
				});
			}
			options.markers = markers;
			$(this).data('options', options);
			
			if (screen.width < options.deviceWidth){
				$(this).mobileGmap('showImage');
			}else{
				$(this).mobileGmap('showMap');
			}
			
		},
		
		showMap : function(){
			var options = $(this).data('options'),
					geocoder = new google.maps.Geocoder(),
					latlng = new google.maps.LatLng(-34.397, 150.644),
					mapOptions = {},
					htmlObj = $(this).get(0);
					geocoder.geocode( { 'address': options.settings.center.replace(/\+/gi, ' ')}, function(results, status) {
					      if (status == google.maps.GeocoderStatus.OK) {
					        // map.setCenter(results[0].geometry.location);
					        mapOptions = {
										zoom: parseInt(options.settings.zoom, 10),
										center: results[0].geometry.location,
										mapTypeId: options.settings.maptype
									}
									var map = new google.maps.Map(htmlObj, mapOptions);
									var marker = new google.maps.Marker({
									            map: map,
									            position: results[0].geometry.location
									        });
					      }
					    });
		},
		
		showImage : function(){
			var par = [],
					r = new Image(),
					l = document.createElement('a'),
					options = $(this).data('options'),
					i = 0,
					m = [];
			for (var o in options.settings){
				par.push(o + '=' + options.settings[o]);
			}
			if (options.markers.length){
				var t=[];
				for (;i < options.markers.length;i++){
					t = [];
					for (var j in options.markers[i]){
						if (j == 'position'){
							t.push(options.markers[i][j]);
						}else{
							t.push(j + ':' + options.markers[i][j]);
						}
					}
					m.push('&markers=' + t.join('%7C'));
				}
			}
			r.src =  options.imgURI + par.join('&') + m.join('');
			l.href = 'http://maps.google.com/maps?q=' + options.settings.center;
			l.appendChild(r);
			$(this).empty().append(l);
		}
		
	};

	$.fn.mobileGmap = function(method){
		if ( methods[method] ) {
					return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
				} else if ( typeof method === 'object' || ! method ) {
					return methods.init.apply( this, arguments );
				} else {
					$.error( 'Method ' + method + ' does not exist on jQuery.mobileGmap' );
		}
	};
})(this.jQuery);


